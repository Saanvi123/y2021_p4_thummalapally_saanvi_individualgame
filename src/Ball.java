
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.util.Duration;

public class Ball extends Actor{
	double dx;
	double dy;
	int x = 0;
	boolean isTouching = false;
	boolean isTouching1 = false;
	int amountOfTimes;
	int ab;
	
	//int amountOfLives;
	
	public Ball(int x, int y, int a) {
		String path = getClass().getClassLoader().getResource("resources/ball.png").toString();
		Image img = new Image(path);
		setImage(img);

		dx = x;
		dy = y;
		amountOfTimes = 0;
		ab = a;
		
	//	amountOfLives = 1;
		
		
		//make an amount of lives feature
		
	}

	@Override
	public void act() {
		
		
		
		ScoreManager.setLives(ScoreManager.getLives());
		((BallWorld) getWorld()).getScore1().setScore(ScoreManager.getLives());
		
		
		ScoreManager.setScore(ScoreManager.getScore());
		((BallWorld) getWorld()).getScore().setScore(ScoreManager.getScore());
		
		
		if(this.getY()<=0) {
			dy = -dy-1;
			
			this.setY(0);
		}
		
		
		if(this.getY()==0 && x==0) {
			this.setY(1);
		}
		if(this.getX()<=0) {
			dx = -dx-1;	
			this.setX(0);
		}
		
		if(this.getX()>=800) {
			dx = -dx-1;
			this.setX(800);
		}
		
		if(this.getY()>= 400) {
			dy =-dy-1;
			this.setY(400);
		}
		
		
		
		
		
		if(Math.random()*100 >98 && ab==1) {	
			getWorld().add(new PowerUpBrick());		
			
		}
		if(Math.random()*100 >99.5 && ab==1){
			
			getWorld().add(new DeathStar());
					
			
		}
		if(Math.random()*100 >99.2 && ab==2){
			
			getWorld().add(new DeathStar());
					
			
		}
		
		
		move(dx, dy);
		
		
		if((getX() + getWidth()/2 >= getWorld().getWidth()) || getX() - getWidth()/2 <= 0){
			dx = -dx;
		} else if(getY() + getHeight()/2 >= getWorld().getHeight() || getY() - getHeight()/2 <= 0){
			if(getY() + getHeight()/2 >= getWorld().getHeight()) {
			//	((BallWorld) getWorld()).getScore().setScore(((BallWorld) getWorld()).getScore().getValue()-1000);
				
				ScoreManager.setScore(ScoreManager.getScore()-1000);
				((BallWorld) getWorld()).getScore().setScore(ScoreManager.getScore());
				
				((BallWorld) getWorld()).getScore().setScoreColor("RED");
			}
			dy = -dy;
		} else if(getY() + getHeight()/2 <= 0 && getX() - getWidth()/2 <= 0){
			dx = -dx;
			dy = -dy;
		} else if (getY() + getHeight()/2 >= getWorld().getHeight() && getX() - getWidth()/2 <= 0){
			dx = -dx;
			dy = -dy;
		} else if(getY() + getHeight()/2 >= getWorld().getHeight() && getX() - getWidth()/2 >= getWorld().getWidth()){
			dx = -dx;
			dy = -dy;
		} else if(getY() + getHeight()/2 <= 0 && getX() - getWidth()/2 >= getWorld().getWidth()){
			dx = -dx;
			dy = -dy;
		
		}
		
		if(getOneIntersectingObject(Paddle.class) == null) isTouching=false;
		else if (getOneIntersectingObject(Paddle.class) != null && isTouching==false) {
			isTouching  = true;
		
			Paddle p = getOneIntersectingObject(Paddle.class);

			if(p.movePattern().equals("nomotion") && getX() + getWidth()/2<=p.getX()+p.getWidth()/2 && getX()-getWidth()/2 >= p.getX()-p.getWidth()/2 ) {
				dy=-dy;
			}
			else if( getX()+getWidth()/2 >= p.getX() + p.getWidth()/3 && getX()-getWidth()/2 >=p.getX()-p.getWidth()/3 ) {
				dy = -dy;
			}
			else if(p.movePattern().equals("left") && getX()-getWidth()/2 >= p.getX()-p.getWidth() && getX()+getWidth()/2 <= p.getX()-getWidth()/6) {
				if(dx>0) dx= -dx;
				dy = -dy;	
			}
			else if(p.movePattern().equals("right")  && getX()-getWidth()/2 >= p.getX()+p.getWidth()/6 && getX()+getWidth()/2 <= p.getX()+getWidth()/2) {	
				if(dx<0) dx= -dx;
				dy = -dy;
			}
			else if(getX()<=p.getX()-p.getWidth()/2) {
				dy= 2*dy;
			}
			else if(getX()>=p.getX()+p.getWidth()/2) {
				dy  =2*dy;
			}


			

			
		} 
		if(getOneIntersectingObject(Bird.class)!=null) {
			
			Bird b = getOneIntersectingObject(Bird.class);
			((BallWorld) getWorld()).getScore().setScore(((BallWorld) getWorld()).getScore().getValue()+1000);
			((BallWorld) getWorld()).getScore().setScoreColor("XXX");
			
			FadeTransition fade = new FadeTransition();
			fade.setDuration(Duration.millis(500));
			
			
			fade.setFromValue(10);
			fade.setToValue(0);
			fade.setCycleCount(1);
			fade.setAutoReverse(false);
			fade.setNode(b);
			
		
			fade.play();
			
			
			//we need to count the duration
			
			fade.setOnFinished(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					getWorld().remove(b);
					
				}
				
				
				
				
			});
		
			
		}
		
		
		if (getOneIntersectingObject(Brick.class) != null){
			Brick b = getOneIntersectingObject(Brick.class);
			
			ScoreManager.setScore(ScoreManager.getScore()+700);
			((BallWorld) getWorld()).getScore().setScore(ScoreManager.getScore());
			
			((BallWorld) getWorld()).getScore().setScoreColor("XXX");
			
			if(getX() + getWidth()/2<=b.getX()+b.getWidth()/2 && getX()-getWidth()/2 >= b.getX()-b.getWidth()/2) {
				dy = -dy;
				FadeTransition fade = new FadeTransition();
				fade.setDuration(Duration.millis(500));
				
				
				fade.setFromValue(10);
				fade.setToValue(0);
				fade.setCycleCount(1);
				fade.setAutoReverse(false);
				fade.setNode(b);
				
			
				fade.play();
				
				
				//we need to count the duration
				
				fade.setOnFinished(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						getWorld().remove(b);
						
					}
					
					
					
					
				});
			}
			else {
				if(getY()+getHeight()/2 <= b.getY()+b.getHeight()/2 && getY()-getHeight()/2 <= b.getY() -b.getHeight()/2) {
					dx = -dx;
				}
				else {
					dx = -dx;
					dy= -dy;
					
				}
				FadeTransition fade = new FadeTransition();
				fade.setDuration(Duration.millis(100));
				
				
				fade.setFromValue(10);
				fade.setToValue(0);
				fade.setCycleCount(1);
				fade.setAutoReverse(false);
				fade.setNode(b);
				
			
				fade.play();
				
				
				//we need to count the duration
				
				fade.setOnFinished(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						getWorld().remove(b);
						
					}
					
					
					
					
				});
			}
			
			
			
		
		}
		
		if(getOneIntersectingObject(PowerUpBrick.class)!=null) {
			
			
			
			ScoreManager.setScore(ScoreManager.getScore()+800);
			((BallWorld) getWorld()).getScore().setScore(ScoreManager.getScore());
			
			getWorld().remove(getOneIntersectingObject(PowerUpBrick.class));
			
	
			
			((BallWorld) getWorld()).getScore().setScoreColor("CYZ");
			dx = -dx;
			
					if(Math.random()<0.7) {
						
						getWorld().add(new InsertPower());
					
					}
					
			
				
				
				
				
			}
			
			
			
		

		if(getOneIntersectingObject(DeathStar.class)!=null) {
			ScoreManager.setLives(ScoreManager.getLives()-1);;
			((BallWorld) getWorld()).getScore1().setScore(ScoreManager.getLives());
	
			System.out.println(ScoreManager.getLives());
			
			
			if(ScoreManager.getLives()==0) {
				getWorld().stop();
				ScoreManager.setLives(1);
			}
			
			getWorld().remove(getOneIntersectingObject(DeathStar.class));
			
		
			
			
		}
		
		
		
		if(getOneIntersectingObject(DeathBrick.class)!=null) {
		
			DeathBrick d= getOneIntersectingObject(DeathBrick.class);
			
			getWorld().remove(d);
			
			
			

			if(getX() + getWidth()/2<=d.getX()+d.getWidth()/2 && getX()-getWidth()/2 >= d.getX()-d.getWidth()/2) {
				dy = -dy;
			}
			else {
				if(getY()+getHeight()/2 <= d.getY()+d.getHeight()/2 && getY()-getHeight()/2 <= d.getY() -d.getHeight()/2) {
					dx = -dx;
				}
				else {
					dx = -dx;
					dy= -dy;
					
				}
			}
			
			Ball ne = new Ball(3, 3, 5);
			ne.setX(200);
			ne.setY(300);
			getWorld().add(ne);
			
			
		}
		else isTouching1 = false;
		
		if(getOneIntersectingObject(DeathStar.class)!=null) {
			getWorld().stop();
			//getWorld().pause();
		}
		
		
		if(getOneIntersectingObject(InsertPower.class)!=null) {
			getWorld().remove(getOneIntersectingObject(InsertPower.class));
			System.out.println(ScoreManager.getScore());
			
			ScoreManager.setScore(ScoreManager.getScore()+1000);
			System.out.println(ScoreManager.getScore());
			((BallWorld) getWorld()).getScore().setScore(ScoreManager.getScore());

			((BallWorld) getWorld()).getScore().setScoreColor("XXX");
			
			if(Math.random()<0.8) {
				ScoreManager.setLives(ScoreManager.getLives()+1);
				((BallWorld) getWorld()).getScore1().setScore(ScoreManager.getLives());
			}
		
			
			
			
			
		}
		

		if((ScoreManager.getScore())<=-8000) {
			ScoreManager.setScore(0);
			getWorld().stop();
			
			
			
		}
		if(ScoreManager.getScore()>=25000) {
			ScoreManager.setScore(0);
			getWorld().win();
		}
		
		
		
		
		
		
		
	}
}  