
import javafx.scene.image.Image;

public class DeathBrick extends Actor{

	public DeathBrick(int x, int y) {
		String path = getClass().getClassLoader().getResource("resources/brick2.png").toString();
		Image img = new Image(path);
		setImage(img);
		
		this.setX(x);
		this.setY(y);
		
	}
	
	@Override
	public void act() {
	}

}