import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javafx.animation.AnimationTimer;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public abstract class World extends Pane{

	private AnimationTimer timer;
	private Set<KeyCode> downKeys = new HashSet<>();
	
	int amountOfBalls;
	boolean running=true;
	
	
	static int highScore = 0;
	int lives = 0;
	
	int amountOfLives = 0;
	

	public World() {

		timer = new AnimationTimer() {

			@Override
			public void handle(long now) {
				for (Actor actor : getObjects(Actor.class)) {
					if(actor.getWorld() != null){
						actor.act();
					}
				}
			}
		};
		amountOfBalls = 0;
		
	}

	public abstract void act();

	public void remove(Actor actor) {
		getChildren().remove(actor);
	//	getChildren().removeAll();
		
	}
	public void removeAll() {
		getChildren().removeAll();
	}

	public void add(Actor actor) {
		getChildren().add(actor);
	}

	public void start() {
		timer.start();
	}

	public void stop() {
		timer.stop();
		
		
		
		getChildren().clear();
		
		
		Text bext = new Text();
		
		String saanvi= "You lost! If you have another \nlevel left, click the button on \n the left to play that level! Good Luck!";
		bext.setText(saanvi);
		bext.setX(0);
		bext.setY(200);
		bext.setFont(new Font(25));
		bext.setFill(Color.WHITE);
		bext.setStroke(Color.YELLOW);
		getChildren().add(bext);
		
	
		
		
	}
	public void win() {
		timer.stop();
		
		
		
		getChildren().clear();
		
		
		Text bext = new Text();
		
		String saanvi= "You won! If you have another \nlevel left, click the button on \n the left to play that level! Good Luck!";
		bext.setText(saanvi);
		bext.setX(0);
		bext.setY(200);
		bext.setFont(new Font(25));
		bext.setFill(Color.WHITE);
		bext.setStroke(Color.YELLOW);
		getChildren().add(bext);
		
	
		
		
	}
	
	public void pause() {
		
		
		try {
			timer.wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
	public void stop(int henlo) {
		timer.stop();
		
		
		
		getChildren().clear();
		
	//	getChildren().clear(button2);	
		
		Text bext = new Text();
		
		String saanvi= "You lost! Your score was "+ henlo;
		bext.setText(saanvi);
		bext.setX(100);
		bext.setY(200);
		bext.setStroke(Color.WHITE);
		getChildren().add(bext);
		
		
		Text dext= new Text();
		
		if(henlo>highScore) {
			String amoun = henlo + "";
			highScore = henlo;
			dext.setText(amoun);
			dext.setX(400);
			dext.setY(300);
			dext.setStroke(Color.GREENYELLOW);
			getChildren().add(dext);
		}
		else {
			String amoun = highScore + "";
		//	highScore = henlo;
			dext.setText(amoun);
			dext.setX(400);
			dext.setY(300);
			dext.setStroke(Color.GREENYELLOW);
			getChildren().add(dext);
			
			
		}
		
		
	}
	

	public <A extends Actor> List<A> getObjects(Class<A> cls){
		ArrayList<A> list = new ArrayList<>();
		for (Node node : getChildren()) {
			if (cls.isInstance(node)) {
				list.add(cls.cast(node));
			}
		}
		return list;
	}

	public void incrementBalls() {
		amountOfBalls++;
	}
	
	public void setLives(int abc) {
		lives = abc;
		
	}
	
	public int getLives() {
		return lives;
		
	}
	
	public int getBalls() {
		return amountOfBalls;
	}
	public void addKeyCode(KeyCode code){
		int x=1;
			downKeys.add(code);
	}

	public void removeKeyCode(KeyCode code){
		downKeys.remove(code);
	}

	public boolean isKeyDown(KeyCode code){
		for (KeyCode i : downKeys) {
			if(i == code){
				return true;
			}
		}
		return false;
}
	public boolean running() {
		return running;
	}
	
	
	public void setRunning() {
		running =  false;
	}
	
	public void setAmountOfLives(int aaa) {
		amountOfLives = aaa;
		
	}
	public int getAmountOfLives() {
		return amountOfLives;
		
	}
	
}//