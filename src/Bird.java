import javafx.scene.image.Image;

public class Bird extends Actor{
	
	double dx;
	double dy;
	
	int counter = 0;
	
	
	
	public Bird(int x, int y) {
		String path = getClass().getClassLoader().getResource("resources/henko.jpg").toString();
		Image img = new Image(path);
		setImage(img);
		
		this.setX(Math.random()*400);
		
		
		
		
		this.setY(Math.random()*150+54);
		
	
		
		dx = x;
		dy= y;
		
	}
	
	@Override
	public void act() {
		counter ++;
		
		if(counter%2==0) {
			move(dx, dy);
		}
		else move(dx, -dy);
		
		
		if(getOneIntersectingObject(Actor.class)!=null) {
			
			Actor a = getOneIntersectingObject(Actor.class);
			getWorld().remove(a);
			getWorld().add(a);
			
		}
		
		
		
		
		
		
		
	}

}
