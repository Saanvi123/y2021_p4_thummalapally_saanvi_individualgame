

import javafx.scene.text.Text;

public class HighScore extends Text {
	
	
	private static int highScore = 0;
	
	
	public  void updateDisplay() {
		setText(highScore + "");
	}
	
	public int getValue() {
		return highScore;
	}
	
	public void setHighScore(int y) {
		
		
		
		if(y>highScore) {
			highScore = y;
			updateDisplay();
		}
	}
	
	
	

}
