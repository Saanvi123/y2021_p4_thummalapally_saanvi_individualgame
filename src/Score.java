import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Score extends Text{
	private  int value;

	public Score( ) {
		value = 0;
		setFont(new Font(20));
		setFill(Color.WHITE);
		updateDisplay();
	}
	
	public void updateDisplay() {
		//setFill(Color.WHITE);
		setText(value + "");
	}
	
	public int getValue() {
		return value;
	}
	
	public void setScore(int x) {
		
		
		if(value>x) {
			value = x;
			setFill(Color.RED);
		}
		else {
			value = x;
			setFill(Color.GREEN);
		}
		
		
			updateDisplay();
			
		
	}
	
	public void setScoreColor(String a) {
		if(a.equals("RED")) {
			setFill(Color.RED);
		}
		else setFill(Color.GREEN);
	}
}
//