import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class BallWorld extends World {
	
	Score score;
	//boolean running = true;
	
	Score score1;
	
	
	
	public BallWorld() {
		
		
		DropShadow dropShadow = new DropShadow();
		dropShadow.setRadius(5.0);
		 dropShadow.setOffsetX(3.0);
		 dropShadow.setOffsetY(3.0);
		 dropShadow.setColor(Color.color(0.4, 0.9, 0.9)); 
		 
		 
		 
		score = new Score();
		score.setEffect(dropShadow);
		score.setCache(true);
		score1 = new Score();
		
		score1.setScore(1);
		
		score.setX(10);
		score.setY(25);
		
		score1.setX(10);
		
		score1.setY(70);
		score1.setFill(Color.GREEN);
		score1.setEffect(dropShadow);
		score1.setCache(true);
		
		Text t = new Text("Amount Of Lives:");
		t.setFill(Color.WHITE);
		

		
		t.setX(0);
		t.setY(50);
		getChildren().add(score);
		
		getChildren().add(score1);
		
		getChildren().add(t);
	}
	
	public Score getScore() {
		return score;
	}
	
	public Score getScore1() {
		return score1;
	}
	
	@Override
	public void act() {


	}
	
	
	
}
//