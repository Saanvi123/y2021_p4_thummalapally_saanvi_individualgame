import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;

public class Paddle extends Actor {
//make a good luck thingy that fades and such?
	
	boolean left = false;
	boolean right= false;
	boolean not = true;
	boolean movement = not;
	
	
	//can i add a pause button? 

	
    public Paddle(){
        String path = getClass().getClassLoader().getResource("resources/paddle.png").toString();
        Image img = new Image(path);
        setImage(img);
    }
    @Override
    public void act() {
        if(getWorld().isKeyDown(KeyCode.LEFT)){
        	
        

        	movement= left;
            move(-10, 0);
        } else if(getWorld().isKeyDown(KeyCode.RIGHT)){

        	movement = right;
            move(10, 0);
            
            
        }
        
        
    }
   
    
    public String movePattern() {
    	
    	if(movement==left) return "left";
    	else if(movement==right) return "right";
    	else if(movement==not) return "nomotion";
    	else return "nomotion";
    }
    
   
    
    
}