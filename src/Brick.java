import javafx.scene.image.Image;

public class Brick extends Actor{
	int dx = 2;
	int dy = -2;

	public Brick(int x, int y) {
		String path = getClass().getClassLoader().getResource("resources/brick.png").toString();
		Image img = new Image(path);
		setImage(img);
		
	//	this.setX(Math.random()*800);
	//	this.setY(Math.random()*400);
		this.setX(x);
		this.setY(y);
	}
	
	@Override
	public void act() {
	}
	
	

}


class MovingBrick extends Brick {
	int counter = 0;
	

	public MovingBrick(int x, int y) {
		super(x, y);
		String path = getClass().getClassLoader().getResource("resources/brick2.png").toString();
		Image img = new Image(path);
		setImage(img);

	}
	
	
	public void act() {
		counter ++;
		
		if(counter%2==0) {
			move(dx, 0);
		}
		else move(dx, 0);
		
		
		
		if(this.getX()>=800 || this.getX() <=0) {
			dx=-dx;
		}
		
		
		
	}
	
	
	
	
}
//x=1