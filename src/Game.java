import java.io.File;
import java.util.List;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.effect.Reflection;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
                                     
public class Game extends Application {

	Scene scene;
	Scene title;
	Scene scene2;
	
	
	Scene instruction1;
	Scene instruction2;
	
	
	//add an end game button to the title page
	
	
	
	
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Game");
		
		ScoreManager sm  = new ScoreManager();
		
		BorderPane root  = new BorderPane();
		BorderPane boot = new BorderPane();
		
		
		  root.setBackground(new Background(new BackgroundFill(Color.BLACK,null, null)));
		  boot.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
		
	
		  
		BallWorld ballWorld  = new BallWorld();
		ballWorld.setPrefWidth(400);
		ballWorld.setPrefHeight(400);

	
		
		BallWorld ballWorld2 = new BallWorld();
		ballWorld2.setPrefWidth(400);
		ballWorld2.setPrefHeight(400);
		
		Ball ball = new Ball(5, 5, 1);
		ball.setX(50);
		ball.setY(90);
		
		
		Ball ball2 = new Ball(5, 5, 2);
		ball2.setX(100);
		ball2.setY(90);
		
	//	DeathStar de = new DeathStar();
		String instructions;
		
		instructions= "The first level is shooting star level. \n The objective of the game is to score as high as possible. Whenever you hit one of the golden stars, occasionally a mystery box opens up. \n When you hit the mysetery box, either the ball speeds up and you gain points, or you gain an extra life. \n Whenever you hit a red star, if the amount of lives is 1, then you lose.\n As part of level 1&2, you will only be using the mouse to move, and not the keys! Good luck! \n If your score is less than -8000 you lose as well\nIf you reach 25,000 points then you win! \n If you either got less than -8000 or more than 25000 points on this level \n your score will be reset for the next. \n There may be some hidden features! ";
		
		
		Text te = new Text(instructions);
		
		
		
		String instructions1;
		instructions1 = "The next level is a big brick buster level. Basically, a few of the bricks on the screen can be hit for points\n and a few when hit for a while causes a new ball to appeaer\n The objective is to gain as much points as possible \n You lose when  your score is too low or when you hit a death star \n The unique thing is that if you hit a brick, and it has not completely faded out yet, and you hit it again, you get points(called  a critical hit).\n Good luck!\n Lose: Score less than -8000 or run out of lives, win: greater than 25,000";
		
		
		Text ne = new Text(instructions1);
		ne.setX(1);
		ne.setY(100);
		
		Paddle paddle = new Paddle();
		paddle.setX(200);
		paddle.setY(300);
		
		
		Paddle paddle2 = new Paddle();
		paddle2.setX(200);
		paddle2.setY(300);

		//Brick brick = new Brick();
	//	MovingBrick be = new MovingBrick(100, 100);
	//	ballWorld.add(be);

		Bird bong = new Bird(1, 1);
		
		
		
		
		Text bext = new Text();
		
		String saanvi= "Welcome to the Shooting Star Game! This is a ball and paddle game, with a few extra additions to it. \n The objective of the game is to score as high as possible, and if you get below a certain score, the game is over \n Or if you run out of lives and hit a death star, the game is also over \n At any point in the game you can restart, exit, or even pause for 2 secs"
				+ "";
		bext.setText(saanvi);
		bext.setLayoutX(10);
		bext.setLayoutY(50);
		
		bext.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 10));
		bext.setFill(Color.WHITE);
		
		
	
		//DeathBrick d = new DeathBrick(99, 43);
		
		Text hext = new Text();
		hext.setX(300);
		hext.setY(300);
		hext.setCache(true);
		
		hext.setText("Shooting Stars: Saanvi Thummalapally");
		hext.setFill(Color.BLACK);
		hext.setFont(Font.font(null, FontWeight.BOLD, 25));
		
		Reflection rel = new Reflection();
		rel.setFraction(0.7f);
		hext.setEffect(rel);
		
	//	hext.setTranslateX(400);
		
		
		
		Brick[] bricks = new Brick[50];
		//DeathBrick[] bocks = new DeathBrick[50];
	MovingBrick[] brings = new MovingBrick[12];
		
		
		for(int i=0; i<=49; i++) {
			bricks[i] = new Brick(i*64+100, 50+5*i);
			
			
			
			
			
		
			ballWorld2.add(bricks[i]);
	
			
		}
		
		
	
		DeathBrick d5 = new DeathBrick(400, 300);
		
		DeathBrick d2 = new DeathBrick(200, 200);
		DeathBrick d3 =new DeathBrick(100, 400);
		
		//ballWorld2.add(d1);
		ballWorld2.add(d2);
		ballWorld2.add(d3);
		ballWorld2.add(d5);
		
	
		
		for(int i=0;i<=11; i++) {
			brings[i] = new MovingBrick((int)(Math.random()*800),(int) (Math.random()*400));
			ballWorld2.add(brings[i]);
		}
		
		MovingBrick bricker = new MovingBrick(100, 200);
		ballWorld2.add(bricker);
		
		MovingBrick bricker2 = new MovingBrick(400, 300);
		ballWorld2.add(bricker2);
		

		ballWorld.add(ball);
		ballWorld.add(paddle);
		
		
		ballWorld2.add(ball2);
		ballWorld2.add(paddle2);
		

		ballWorld.add(bong);
	//	ballWorld.add(d);
		
		
		Text tee = new Text("Good Luck!!!");
		tee.setFont(new Font(25));
		tee.setFill(Color.NAVY);
		tee.setX(300);
		tee.setY(200);
		
		
		FadeTransition fade = new FadeTransition();
		fade.setDuration(Duration.millis(2000));
		
		
		fade.setFromValue(10);
		fade.setToValue(0);
		fade.setCycleCount(10000);
		fade.setAutoReverse(false);
		fade.setNode(tee);
		
	
		fade.play();
		
		
		//we need to count the duration
		
		
		
		Button button1 = new Button("Go to from Title Page to Level 1 Instructions");
		button1.setLayoutX(300);
		button1.setLayoutY(200);
		
		
		Button button4 = new Button("Go from instructions page to level 2");
		
		Button button6 = new Button("Pause the Game(2 seconds)");
		button6.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			
			
			
			
		});
		
		
		button4.setLayoutX(300);
		button4.setLayoutY(300);
		button4.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				stage.setScene(scene2);
				
				ballWorld2.start();
				
				
				
			}
			
			
			
			
		});
		
		
		
		
		
		
		Button button2  = new Button("next");
	//	button2.setPrefSize(10, 10);
	//	button2.setLayoutX(300);
	//	button2.setLayoutY(200);
		
		
		//button1.setOnAction(e -> stage.setScene(scene), ballWorld.start(););
		
		button1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				stage.setScene(instruction1);
				//ballWorld.start();
				
			}
			
			
			
			
		});
		
		
		
		Button button3= new Button("Go to the shooting star ");
		
		button3.setLayoutX(300);
		button3.setLayoutY(200);
		button3.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				stage.setScene(scene);
				
				ballWorld.start();
				
				
				
			}
			
			
			
			
		});
		
		Button button7 = new Button("Exit!");
		button7.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
				
			//	ballWorld2.start();
				
				
			}
			
			
			
			
		});
		
		
		
		Button button5 = new Button("End Game:");
		
		button5.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
				
			//	ballWorld2.start();
				
				
			}
			
			
			
			
		});
		
		Button button11 = new Button("Reset Button");
		
		button11.setOnAction(new EventHandler<ActionEvent>() {
			
			public void handle(ActionEvent event) {
				
				try {
					start(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				
			}
			
		});
		Button button13 = new Button("Reset Button");
		
		button13.setOnAction(new EventHandler<ActionEvent>() {
			
			public void handle(ActionEvent event) {
				
				try {
					start(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				
			}
			
		});
		
		
		
		
		button2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				stage.setScene(instruction2);
			//	ballWorld2.start();
				
				
			}
			
			
			
			
		});
		
		Button buttone = new Button("Pause Button");
		buttone.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			
			
			
			
		});
		
		Button guttone = new Button("Exit");
		guttone.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
				
			//	ballWorld2.start();
				
				
			}
			
			
			
			
		});
		
		
		
		
		
		
		
	
		
		
		
		
		
		
		
		
		if(ballWorld.running()==false) {
			stage.setScene(title);
			ballWorld.stop();
		}
		
			
		
		Group root1 = new Group();
		
		
		Group bootie = new Group();
		
		Group cootie = new Group();
		
		title = new Scene(root1, 800, 400, Color.CORAL);
		 instruction1 = new Scene(bootie, 800, 400, Color.LIGHTBLUE);
		 instruction2 = new Scene(cootie, 800, 400, Color.LIGHTGREEN);
		 
		
		 
		 
		root1.getChildren().add(button1);
		root1.getChildren().add(bext);
		root1.getChildren().add(hext);
		
		 bootie.getChildren().add(te);
		 bootie.getChildren().add(button3);
		 bootie.getChildren().add(tee);
		 cootie.getChildren().add(ne);
		 cootie.getChildren().add(button4);
		 

		root.setCenter(ballWorld);
		boot.setCenter(ballWorld2);
		ballWorld2.setOnMouseMoved(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
			
				paddle2.setX(event.getX());
			}});

		
	
		ballWorld.setOnMouseMoved(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
			
				paddle.setX(event.getX());
			}});
		
		 scene = new Scene(root,800, 400);
		 scene2 = new Scene(boot, 800, 400);
	
		 
		 
		 stage.setResizable(false);
		 
		 FlowPane buttons = new FlowPane() ;
		 buttons.setAlignment(Pos.CENTER);;
		 root.setTop(buttons);
		 
		 buttons.getChildren().addAll(button2, button6, button7, button11);
		 
		 
		 
		 
		 FlowPane buttons2 = new FlowPane();
		 buttons2.setAlignment(Pos.CENTER);
		 boot.setTop(buttons2);
	
		 
		 buttons2.getChildren().addAll(buttone, button13, guttone);
		 
		 
		 
		 
		 
		 
	//	root.setLeft(buttons);
		
//		boot.setLeft(button10);
		
		
		
	
		stage.setScene(title);
		
		
		stage.show();
		ballWorld.requestFocus();

		ballWorld2.requestFocus();
	/*	
		ballWorld.setOnKeyPressed(new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent keyEvent) {
				KeyCode code = keyEvent.getCode();
				if(code == KeyCode.LEFT){
					ballWorld.addKeyCode(KeyCode.LEFT);
				} else if(code == KeyCode.RIGHT){
					ballWorld.addKeyCode(KeyCode.RIGHT);
				}
			}
		});

		ballWorld.setOnKeyReleased(new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent keyEvent) {
				KeyCode code = keyEvent.getCode();
				if(code == KeyCode.LEFT){
					ballWorld.removeKeyCode(KeyCode.LEFT);
				} else if(code == KeyCode.RIGHT){
					ballWorld.removeKeyCode(KeyCode.RIGHT);
				}
			}
		});*/
		/*
		
		ballWorld2.setOnKeyPressed(new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent keyEvent) {
				KeyCode code = keyEvent.getCode();
				if(code == KeyCode.LEFT){
					ballWorld2.addKeyCode(KeyCode.LEFT);
				} else if(code == KeyCode.RIGHT){
					ballWorld2.addKeyCode(KeyCode.RIGHT);
				}
			}
		});

		ballWorld2.setOnKeyReleased(new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent keyEvent) {
				KeyCode code = keyEvent.getCode();
				if(code == KeyCode.LEFT){
					ballWorld2.removeKeyCode(KeyCode.LEFT);
				} else if(code == KeyCode.RIGHT){
					ballWorld2.removeKeyCode(KeyCode.RIGHT);
				}
			}
		});

		//ballWorld.start();
		 * */
		 
	}
}//