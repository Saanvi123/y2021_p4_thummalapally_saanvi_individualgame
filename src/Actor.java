import java.util.ArrayList;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

public abstract class Actor extends ImageView{

	public abstract void act();

	public void move(double dx, double dy) {
		setX(dx + getX());
		setY(dy + getY());
	}

	public World getWorld() {
		if(getParent() != null) return (World) getParent();
		else return null;
	}

	public double getWidth() {
		Bounds bounds = getBoundsInParent();
		return bounds.getWidth();
	}

	public double getHeight() {
		Bounds bounds = getBoundsInParent();
		return bounds.getHeight();
	}

	public <A extends Actor> java.util.List<A> getIntersectingObjects(java.lang.Class<A> cls){
		ArrayList<A> list = new ArrayList<>();
		for (int i = 0; i < getWorld().getChildren().size(); i++) {
			Node node = getWorld().getChildren().get(i);
			if (node != this && node.intersects(getBoundsInLocal()) && cls.isInstance(node)) {
				list.add(cls.cast(node));
			}
		}
		return list;
	}

	public <A extends Actor> A getOneIntersectingObject(java.lang.Class<A> cls){
		if(getIntersectingObjects(cls).size()<=0) return null;
		return getIntersectingObjects(cls).get(0);
	}
}
//